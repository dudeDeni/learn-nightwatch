const config = require('../../nightwatch.conf.js');

module.exports = {
  // eslint-disable-next-line object-shorthand
  Nightwatch: function (browser) {
    browser
      .url('https://nightwatchjs.org/')
      .waitForElementVisible('body')
      .assert.title('Nightwatch.js | Node.js powered End-to-End testing framework')
      .saveScreenshot(`${config.imgpath(browser)}nightwatch.png`)
      .click('#index-container > div > div.parallax__layer.parallax__layer--0 > header > div > button')
      .pause(500)
      .assert.containsText('#navigation', 'Developer Guide')
      .useXpath()
      .click('//*[@id="navigation"]/ul/li/a[text()="Developer Guide"]')
      .pause(500)
      .useCss()
      .waitForElementVisible('body')
      .assert.containsText('body', 'Writing Tests')
      .saveScreenshot(`${config.imgpath(browser)}nightwatch-developer-guide.png`)
      .end();
  },
};
