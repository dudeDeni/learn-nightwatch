const config = require('../../nightwatch.conf.js');

module.exports = {
  '1a': function (browser) {
    browser
      .url('https://www.1a.ee/')
      .windowSize('current', 1200, 1200)
      .waitForElementVisible('body')
      .assert.visible("input[type='text']")
      .setValue('#q', 'nightwatch testing!')
      .saveScreenshot(`${config.imgpath(browser)}1a_input.png`)
      .useXpath()
      .click('/html/body/div[2]/div[3]/div[1]/div/div[1]/ul/li[8]')
      .pause(500)
      .click('/html/body/div[2]/div[3]/div/div/div/div/div[2]/div[2]/div[3]/ul/li[3]/a/span')
      .useCss()
      .assert.containsText('.senukai-v2', 'Lauad')
      .useXpath()
      .click('/html/body/div[2]/div[3]/div/div/div/div/div/div/div/div[2]/div[1]/div[1]/ul/li[2]/ul/li[2]/a')
      .pause(500)
      .saveScreenshot(`${config.imgpath(browser)}1a_tables.png`)
      .end();
  },
};
