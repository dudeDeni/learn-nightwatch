/*

[x] open delfi.ee
[x] pealkiri on "DELFI - Värsked uudised Eestist ja välismaalt - DELFI"
[x] vota screenshot
[x] page contains Podcast link
[] klikkides podcast lingi peale avaneb uus sait mille peakiri on "Delfi tasku"
[] vota screenshot

*/

const config = require('../../nightwatch.conf.js');

module.exports = {
  'Delfi Podcast': function (browser) {
    browser
      .url('https://www.delfi.ee/')
      .windowSize('current', 1200, 1200)
      .waitForElementVisible('body')
      .pause(15000)
      .assert.title('Delfi')
      .saveScreenshot(`${config.imgpath(browser)}delfi-esileht.png`)
      .useXpath()
      .click('//*[@id="C-header-menu--delfi-menuu"]/div/div/div/div/div/ul/li[9]/a')
      .pause(1000)
      .useCss()
      .waitForElementVisible('body')
      .assert.title('Delfi Tasku')
      .saveScreenshot(`${config.imgpath(browser)}delfi-tasku.png`)
      .end();
  },
};
